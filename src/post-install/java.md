# Java Development Kit (JDK)
Java is a is a general-purpose programming language that is taught in first year Computer Science at UoB. The development kit is the package you need in order to compile and run Java applications.

**Notice:** Java and JavaScript are two very different languages - it has as much in common as a Car and a Carpet.

## Installation
`sudo apt install default-jdk`

## Running
In order to verify that Java has been installed correctly, run the following command.

`java --version`

This should produce the following (or similar) output if working correctly.
![../images/java-version.png](../images/java-version.png)

## Multiple Java Versions
The first year CS course at UoB requres submissions compatible with Java 11, and so recommend that this is what's used for assignments. In general though it is a good idea to have the latest version available. In this case it's useful to be able to manage multiple installed java versions on one PC. It may well be that the latest version of Java that comes with your distriubtion is Java 11 (as with Ubuntu 20.04), in which case you can ignore this section.

Assuming you've already installed the latest JDK as above, install JDK 11:

`sudo apt install openjdk-11-jdk`

Once the new version is installed, you will be able to see all installed versions like this:

`update-alternatives --list java`

And switch between them like this:

`sudo update-alternatives --config java`

## Removal
`sudo apt remove default-jdk`

## Installing Eclipse

You may want to install Eclipse, the IDE recommended by the School.
The easiest way to do this, is to install it through `snap`.

To install Eclipse:

`sudo snap install eclipse --classic`

After Eclipse is installed, you might want to check for updates.
From Eclipse, go to Help -> Check for updates. Select the updates
you want to do, click Next, confirm, agree with the Terms and 
Conditions and click Finish.

You may want to install other software/plugins. Can do this from
Help -> Install New Software. Select the URL corresponding to
the Eclipse version you have and the list will get populated
with possible software you may want to install.

![Eclipse Software](../images/eclipse_software.png)

## Eclipse on Apple Silicon Linux
This works but must be done via the offcial downloader [here](https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2023-09/R/eclipse-java-2023-09-R-linux-gtk-aarch64.tar.gz).
