# UFW

UFW means Uncomplicated FireWall and don't worry, it is uncomplicated. With UFW you simply describe firewall rules in the simplest possible way and it applies them, permanently. By default it applies to all protocols and interfaces, you can be more specific but the default is the safest, this means it protects you via wifi, ethernet and over VPNs (it doesn't affect the local interface however).

## Installation
`sudo apt install ufw`

## Checking the status

To see what firewall rules you have running run the command:

`sudo ufw status verbose`

It will tell you it's inactive for now.

To permanently activate the firewall run:

`sudo ufw enable`

Check the status again and it should be running.

## Basic "Deny all" Rule

The simplest security model here is simply to deny every incoming connection attempt. Unless you have a very good reason for why you / someone else needs to be able to connect to your laptop remotely over a local network this will be fine.

So all we're going to do is write a single rule that sets the default to do this:

`sudo ufw default deny incoming`

Now any connection attempts to your ports will be rejected.

## Adding exceptions

In special circumstances you might want to allow specific services to be reached throught the firewall, for example maybe you want someone to test a server running on your laptop.

Here you simply add a rule on top of the default that says "allow connections for this port number", and now you've got a nice precise hole in your firewall.

So to do this you can run the command:

`sudo ufw allow in <PORT NUMBER>/tcp`

You can switch TCP for UDP if that's what you need.

It's good practice to disable tis after you stop needing it, in case in the future you run something less secure on the same port and forget to reenable the firewall.

To do this you can simply run the same command as above but with the delete keyword after ufw: this will tell UFW to delete the rule described.

`sudo ufw delete allow in <PORT NUMBER>/tcp`

## Finally and Further Reading

Now you're good to go! Remember to check the status after each new rule just to check that it did what you want.

This is a good guide to UFW: it goes over many of the same steps but some others as well if you're interested:
[DigitalOcean UFW Guide](https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server)

And remember `man ufw` is your friend and will help you out with how to write complicated commands.
