# Troubleshooting 


For technical questions about Linux and support after install, join the `#tech-support` channel in the [CSS Discord](https://discord.gg/vQD4nZ6) and/or the [AFNOM Discord](https://discord.gg/ANZXYwzMey).

Note that these discord servers are only available to current UoB CS students.

